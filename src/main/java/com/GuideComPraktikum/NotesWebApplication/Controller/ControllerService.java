package com.GuideComPraktikum.NotesWebApplication.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ControllerService {
    @GetMapping("/")
    public String getHomePage(){
        return "index";
    }
}
