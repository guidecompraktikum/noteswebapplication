package com.GuideComPraktikum.NotesWebApplication.Controller;

import com.GuideComPraktikum.NotesWebApplication.Models.Note;
import com.GuideComPraktikum.NotesWebApplication.Models.NoteUser;
import com.GuideComPraktikum.NotesWebApplication.Services.BusinessLogic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@RestController
public class RestControllerService {
    @Autowired
    BusinessLogic businessLogic;

    @GetMapping("addNote")
    private boolean createNote(String pTitle, String pContent, String pUsername, int pImportance)
    {
        if (businessLogic.findUserViaUsername(pUsername) == null){businessLogic.addUser(pUsername);}
        if (businessLogic.findNoteViaTitle(pTitle) == null){
            businessLogic.addNote(pTitle, pContent, pUsername, pImportance);
            return true;
        }
        else{ return false;}
    }

    @GetMapping("/getNoteTitleList")
    @ResponseBody
    public List<String> getNoteList() {
        return businessLogic.getAllNotes().stream()
                .map(Note::getNoteTitle)
                .collect(Collectors.toList());
    }

    @GetMapping("getNoteDetail")
    private String getNoteDetail(String pTitle)
    {
        Note thisNote = businessLogic.findNoteViaTitle(pTitle);
        if (thisNote == null) return "False";
        return thisNote.getNoteTitle() + ":" + thisNote.getNoteContent() + ":" + thisNote.getCreationDate()
                + ":" + thisNote.getCreatedByNoteUser() + ":" + thisNote.getNoteImportance();
    }

    @GetMapping("generateTestData")
    private void generateTestData()
    {
        for (int i = 1; i <= 20; i++){
            Random rnd = new Random();
            businessLogic.addUser("User " + i);
            businessLogic.addNote(("Note no " + i), ("This is the extreme complex and long content of Note " + i), ("User " + 1), rnd.nextInt(3));
        }

        System.out.println("Test data saved!");
    }

    @GetMapping("setLastRequestedNote")
    private void setLastRequestedNote(String pTitle)
    {
        businessLogic.setLastRequestedNote(businessLogic.findNoteViaTitle(pTitle));
    }
    @GetMapping("getLastRequestedNote")
    private String getLastRequestedNote()
    {
        return businessLogic.getLastRequestedNote().getNoteTitle();
    }

    @GetMapping("test")
    private String testg(String test)
    {
        return test;
    }

    @GetMapping("deleteNote")
    private void deleteNote(String pTitle){
        deleteNote(pTitle);
    }

    @GetMapping("editNote")
    private void editNote(String pTitle, String pNewContent){
        editNote(pTitle, pNewContent);
    }
}
