package com.GuideComPraktikum.NotesWebApplication.Repository;

import com.GuideComPraktikum.NotesWebApplication.Models.Note;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NoteRepository extends JpaRepository<Note, String> {
}