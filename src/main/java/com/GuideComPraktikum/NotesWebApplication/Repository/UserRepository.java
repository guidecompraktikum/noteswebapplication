package com.GuideComPraktikum.NotesWebApplication.Repository;

import com.GuideComPraktikum.NotesWebApplication.Models.NoteUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<NoteUser, String> {
}
