package com.GuideComPraktikum.NotesWebApplication.Services;

import com.GuideComPraktikum.NotesWebApplication.Models.Note;
import com.GuideComPraktikum.NotesWebApplication.Models.NoteUser;
import com.GuideComPraktikum.NotesWebApplication.Repository.NoteRepository;
import com.GuideComPraktikum.NotesWebApplication.Repository.UserRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BusinessLogic {
    @Autowired
    NoteRepository noteRepository;
    @Autowired
    UserRepository userRepository;

    @Getter
    @Setter
    Note lastRequestedNote = null;


    // ****** User ******
    public List<NoteUser> getAllUser(){return userRepository.findAll();}
    public NoteUser findUserViaUsername(String pUsername){
        for(NoteUser thisUser : getAllUser()){
            if (thisUser.getUsername() == pUsername) return thisUser;
        }
        return null;
    }
    public void addUser(String pUsername){
        if (findUserViaUsername(pUsername) == null){
            NoteUser thisUser = new NoteUser(pUsername);
            userRepository.save(thisUser);
            System.out.println("New User created: " + pUsername);
        }
    }


    // ****** Notes ******
    public List<Note> getAllNotes(){return noteRepository.findAll();}
    public Note findNoteViaTitle(String pTitle){
        for(Note thisNote : getAllNotes()){
            if (thisNote.getNoteTitle().equals(pTitle)) return thisNote;
        }
        return null;
    }
    public void addNote(String pTitle, String pContent, String pUsername, int pImportance)
    {
        Note newNote = new Note(pTitle, pContent, findUserViaUsername(pUsername), pImportance);
        noteRepository.save(newNote);
        System.out.println("New Note created: " + pTitle);
    }
    public void deleteNote(String pTitle){
        noteRepository.delete(findNoteViaTitle(pTitle));
    }
    public void editNote(String pTitle, String pNewContent){
        noteRepository.delete(findNoteViaTitle(pTitle));
        findNoteViaTitle(pTitle).setNoteContent(pNewContent);
        noteRepository.save(findNoteViaTitle(pTitle));
    }
}
