package com.GuideComPraktikum.NotesWebApplication.Models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

@Setter
@Getter
@Entity
@NoArgsConstructor
public class NoteUser implements Serializable {
    public NoteUser (String pUsername)
    {
        username = pUsername;

        // Setzen Sie das Erstellungsdatum auf das aktuelle Datum
        LocalDateTime localDateTime = LocalDateTime.now();
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        creationDate = localDateTime.toLocalDate().format(dateFormatter);
    }



    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String username, creationDate;
}
