package com.GuideComPraktikum.NotesWebApplication.Models;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Random;

enum NoteImportance{Low, Medium, High}

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Note implements Serializable {
    public Note(String pTitle, String pContent, NoteUser pUser, int pImportance)
    {
        noteTitle = pTitle;
        noteContent = pContent;
        createdByNoteUser = pUser;

        // Setzen Sie das Erstellungsdatum auf das aktuelle Datum
        LocalDateTime localDateTime = LocalDateTime.now();
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        creationDate = localDateTime.toLocalDate().format(dateFormatter);

        switch (pImportance){
            case 0:
                noteImportance = NoteImportance.Low;
                break;
            case 1:
                noteImportance = NoteImportance.Medium;
            case 2:
                noteImportance = NoteImportance.High;
        }
    }



    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String noteContent, noteTitle, creationDate;
    private NoteImportance noteImportance;
    private NoteUser createdByNoteUser;
}
