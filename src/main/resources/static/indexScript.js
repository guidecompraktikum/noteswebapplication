document.addEventListener("DOMContentLoaded", function() {
    // ******* Variables *******
    const formTitleInput = document.getElementById("titleFormInput");
    const formContentInput = document.getElementById("contentFormInput");
    const form = document.getElementById("form");
    const noteTitleList = document.getElementById("noteList");

    const noteTitles = fetchData("/getNoteTitleList");
    updateVisuals(noteTitles);

    function updateVisuals(noteTitles) {
        while (noteTitleList.firstChild) {
            noteTitleList.removeChild(noteTitleList.firstChild);
        }

        noteTitles.forEach(function(thisElement) {
            const thisListElement = document.createElement("li");
            const thisListElementChild = document.createElement("div");
            const thisListElementChildButtonDelete = document.createElement("button");
            const thisListElementChildButtonEdit = document.createElement("button");

            thisListElement.className = "note-item d-flex justify-content-between";
            thisListElement.textContent = thisElement;

            thisListElementChildButtonDelete.className = "btn btn-danger me-2";
            thisListElementChildButtonDelete.textContent = "Delete";

            thisListElementChildButtonEdit.className = "btn btn-secondary";
            thisListElementChildButtonEdit.textContent = "Edit";

            thisListElementChild.appendChild(thisListElementChildButtonDelete);
            thisListElementChild.appendChild(thisListElementChildButtonEdit);
            thisListElement.appendChild(thisListElementChild);
            noteTitleList.appendChild(thisListElement);

            thisListElementChildButtonDelete.addEventListener("click", function() {
                // Implement delete functionality
            });

            thisListElementChildButtonEdit.addEventListener("click", function() {
                // Implement edit functionality
            });
        });
    }

    function fetchData(url) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', url, false); // Synchronous request
        xhr.send(null);

        if (xhr.status === 200) {
            try {
                return JSON.parse(xhr.responseText);
            } catch (e) {
                console.error("Failed to parse JSON response:", xhr.responseText);
                throw new Error('Response is not valid JSON');
            }
        } else {
            throw new Error('Request failed: ' + xhr.statusText);
        }
    }
});
